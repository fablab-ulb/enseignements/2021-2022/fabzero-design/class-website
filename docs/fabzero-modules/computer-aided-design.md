## Conception assistée par ordinateur

## Objectif de ce module

Dans ce module, vous apprendrez à

* Démontrer et décrire les processus utilisés dans la modélisation avec un logiciel 3D.

## Matériel de cours

* [Fusion 360](https://www.autodesk.fr/products/fusion-360/personal)

## Devoir

* Modélisez l'objet ou une partie de l'objet que vous avez choisi comme point de départ de l'option.
* Faites-le paramétriquement avec au minimum un paramètre, de sorte que vous puissiez faire des ajustements automatiquement.
* Documentez le processus de modélisation à l'aide de captures d'écrans et d'images.
