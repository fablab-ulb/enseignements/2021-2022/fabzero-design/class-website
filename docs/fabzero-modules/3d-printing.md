## Impression 3D

## Objectif de ce module

Dans ce module, vous apprendrez à

* Identifier les avantages et les limites de l'impression 3D.
* Appliquer des méthodes de conception et des processus de production utilisant l'impression 3D.


## Exercice

* Testez les règles de conception de votre (vos) imprimante(s) 3D.
* Imprimez en 3D votre objet choisi ou une partie de votre objet en maximum 2h d'impression. Pour les personnes travaillant au musée, imprimez l'objet ou une partie de l'objet choisi au musée.
* Documentez le processus de fabrication à l'aide de captures d'écrans et d'images.
