## Découpe Assistée par Ordinateur

## Objectif de ce module

Dans ce module, vous apprendrez à

* Concevoir, préparer des fichiers CAO 2D et les découper au laser et à la découpeuse vinyle.

## Matériel de cours

* [Laser Cutters](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)
* [Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)

## Exercice

Travail de groupe :

* Caractérisez la focalisation, la puissance, la vitesse, la largeur de trait, etc. de votre découpeuse laser pour le pliage et la découpe de polypropylène (PP). ([exemple ici](https://fabacademy.org/2020/labs/ulb/assignments/week04/))

Travail individuel :

* Concevez, découpez au laser et documentez un découpage pour fabriquer une lampe en 3D.
* Concevez et fabriquer une ou plusieurs lampes à partir d’une feuille de Polypropylène translucide (50 x 80 cm) en partant du thème de l’objet que vous avez choisi. Le moins de chute possible, pas de colle, pas de vis, ne rien ajouter à la lampe.
* Découpez un auto-collant 2D (max 5x5cm) à l’aide d’une découpeuse vinyle.
