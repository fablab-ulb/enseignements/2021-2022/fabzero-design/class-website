## Documenter votre travail

## Objectif de cette unité

Le but de cette unité est d'apprendre quelques outils simples mais puissants pour écrire une meilleure documentation et la partager en utilisant des systèmes de contrôle de version.

## Matériel de classe

### Mind map

<embed src="../docs-tube.pdf?zoom=100" width="100%" height="500px">
</embed>

### Liens rapides

Pour vous inspirer :

* [2020-2021 FabZero Design](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/class-website/)
* [2020-2021 FabZero Experiments](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/)

Docs de la journée de formation :

* [Command Line Interface CLI](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/command-line.md)
* [Docs](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#why-is-documentation-important-)
* [Comment installer Linux sur un ordinateur windows (optionnel)](https://dtwg4.github.io/blog-flexible-jekyll/installer-linux/)
* [Accès à l'espace de classe FabZero-Design sur GitLab](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design)

Pour ceux qui n'ont pas encore de GitLab ID :  
*forkez* ce projet sur votre espace personnel (sur gitlab.com) : https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/firstname.surname
et travaillez dessus.


## Exercice

* suivez un tutoriel git
* utiliser git pour construire un site personnel dans l'espace gitlab de classe vous décrivant et présentant l'objet que vous avez choisi pour votre projet final.
* documentez votre parcours d'apprentissage pour ce module.
